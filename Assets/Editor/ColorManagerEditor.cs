﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(ColorManager))]
public class ColorManagerEditor : Editor
{
    private ReorderableList _colorList;

    private void OnEnable()
    {
        _colorList = new ReorderableList(serializedObject, serializedObject.FindProperty("levelColors"), true, true, true, true);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        _colorList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = _colorList.serializedProperty.GetArrayElementAtIndex(index);

            rect.y += 3;
            EditorGUI.LabelField(new Rect(rect.x, rect.y,40, EditorGUIUtility.singleLineHeight),"Main:");
            EditorGUI.PropertyField(new Rect(rect.x+40, rect.y, 40, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("mainColor"), GUIContent.none);
            EditorGUI.LabelField(new Rect(rect.x+90, rect.y, 65, EditorGUIUtility.singleLineHeight), "Inversion:");
            EditorGUI.PropertyField(new Rect(rect.x+155, rect.y, 40, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("inversionColor"), GUIContent.none);
        };
        _colorList.drawHeaderCallback = (Rect rect) => { EditorGUI.LabelField(rect, "Colors"); };
        _colorList.DoLayoutList();

        serializedObject.ApplyModifiedProperties();
    }
}
