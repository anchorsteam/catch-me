﻿using UnityEngine;
using System.Collections;

public class Spark : MonoBehaviour
{
    public float lifeTime;
    
    void Awake()
    {
        GetComponent<ParticleSystem>().startColor = ColorManager.singletone.currentColor.inversionColor;
    }

    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
            Destroy(gameObject);
    }
}
