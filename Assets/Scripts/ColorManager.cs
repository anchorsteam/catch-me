﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class LevelColor
{
    public Color mainColor;
    public Color inversionColor;
}

[Serializable]
public class ColorManager : MonoBehaviour
{
    public static ColorManager singletone;
    public List<LevelColor> levelColors;

    public LevelColor currentColor;

    void Awake()
    {
        singletone = this;
        NewColors();           
    }

    public void NewColors()
    {
        currentColor = levelColors[UnityEngine.Random.Range(0, levelColors.Count)];

        currentColor.inversionColor.a = 1;
        currentColor.mainColor.a = 1;
    }

    public void SetColor(GameObject obj,float alpha)
    {
        obj.GetComponent<SpriteRenderer>().color = new Color(currentColor.inversionColor.r, currentColor.inversionColor.g, currentColor.inversionColor.b,alpha);
    }
}
