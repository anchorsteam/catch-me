﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    
    public bool OnDestroy;

    private bool _onCreate;

    private float _timer;

    private SpriteRenderer _sr;

    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        ColorManager.singletone.SetColor(gameObject,0);
        _onCreate = true;
    }

	void Update ()
    {
        _timer += Time.deltaTime/2;
        if (_onCreate)
            _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b,  Mathf.Lerp(0,1,_timer));
        if(OnDestroy)
            _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, Mathf.Lerp(1, 0, _timer));

        if(_timer>=1)
        {
            _onCreate = false;
            if (OnDestroy)
            {
                GameController.singleton.AddObstacle();
                Destroy(gameObject);                
            }
        }        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        DestroyObstacle();
    }

    public void DestroyObstacle()
    {
        _timer = 0;
        OnDestroy = true;
        GetComponent<Collider2D>().isTrigger = true;
    }
}
