﻿using UnityEngine;
using VoxelBusters.NativePlugins;
using VoxelBusters.Utility;
public class GameController : MonoBehaviour
{
    public GameObject Line;
    public GameObject Player;
    public GameObject[] Obstacles;
    public float MinimalLineSize;

    public ShopViewer ShopContent;

    [HideInInspector]
    public static GameController singleton;

    public int Score;

    private GameObject _currentLine;
    private GameObject _bufferLine;
    private GameObject _player;
    private GameObject _obstacle;

    private Vector2 _startPos;

    private bool _inGame;
    private bool _waitSecondDot;

    private AudioSource _butttoAudioSource;    

    public void PlayButtonSound()
    {
        _butttoAudioSource.Play();
    }

    void Awake()
    {
        singleton = this;
        _butttoAudioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        Camera.main.backgroundColor = ColorManager.singletone.currentColor.mainColor;
        _inGame = false;
        
        
    }

    public void StartGame()
    {
        UIController.singleton.ActivateGamePanel();
        _player = Instantiate(Player)as GameObject;
        Score = 0;
        UIController.singleton.UpdateScore(0);
        UpdateColor();
        _inGame = true;
    }

    void Update()
    {
        if (_inGame)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!_waitSecondDot)
                {
                    _startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    _bufferLine = Instantiate(Line, _startPos, Quaternion.identity) as GameObject;
                    _bufferLine.transform.localScale = Vector2.zero;
                }
            }
            if (Input.GetMouseButton(0))
            {
                Vector2 currentPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (Vector2.Distance(_startPos, currentPos) > MinimalLineSize)
                {
                    _bufferLine.transform.localScale = new Vector3(Vector2.Distance(_startPos, Camera.main.ScreenToWorldPoint(Input.mousePosition)), Line.transform.localScale.y, Line.transform.localScale.z);
                    _bufferLine.transform.position = (_startPos + currentPos) / 2;
                    _bufferLine.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(_startPos.y - currentPos.y, _startPos.x - currentPos.x) * (180 / Mathf.PI));
                    _bufferLine.GetComponent<Line>().SetRightEdge(currentPos);
                    _waitSecondDot = false;
                }
                else
                    _waitSecondDot = true;
            }
            if (Input.GetMouseButtonUp(0)&&_bufferLine!=null)
            {
                if (!_waitSecondDot)
                {
                    Destroy(_currentLine);

                    _currentLine = _bufferLine;
                    _currentLine.GetComponent<Line>().SetLine();
                    _bufferLine = null;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_inGame)
                Pause();
            else
                Application.Quit();
            PlayButtonSound();
        }
    }

    public void AddObstacle()
    {
        Vector2 obstaclePosition;
        do
        {
            obstaclePosition = new Vector2(Random.Range(-2.1f, 2.1f), Random.Range(-4.3f, 4.3f));
        }
        while (Vector2.Distance(_player.transform.position, obstaclePosition) <= 2f);
        
        _obstacle = Instantiate(Obstacles[Random.Range(0, Obstacles.Length)], obstaclePosition, Quaternion.Euler(0, 0, Random.Range(0, 360))) as GameObject;
        _obstacle.transform.localScale *= Random.Range(1,1.5f);
    }

    public void AddPoint()
    {
        Score++;
        ColorManager.singletone.NewColors();
        UpdateColor();

        UIController.singleton.UpdateScore(Score);

        if (Score % 5 == 0 && _obstacle&& !_obstacle.GetComponent<Obstacle>().OnDestroy)
            _obstacle.GetComponent<Obstacle>().DestroyObstacle();
        else if (Score % 5 == 0 && !_obstacle)
            AddObstacle();

        if(Score%10==0)
            Shop.money++;

        UIController.singleton.UpdateCoinsCount();
    }

    public void UpdateColor()
    {
        Camera.main.backgroundColor = ColorManager.singletone.currentColor.mainColor;

        if(_currentLine)
            _currentLine.GetComponent<Line>().UpdateColor(1);
        if(_bufferLine)
            _bufferLine.GetComponent<Line>().UpdateColor(0.6f);
        if(_obstacle)
            ColorManager.singletone.SetColor(_obstacle, 1);

        ColorManager.singletone.SetColor(_player, 1);

        UIController.singleton.UpdateUIColor();

        ShopContent.UpdateButtonsInfo();
    }

    public void GameOver()
    {
        UIController.singleton.ActivateGameOverPanel();
        UIController.singleton.CheckScore(Score, PlayerPrefs.GetInt("BestScore"));
        Shop.mainShop.Save();
        if (Score > PlayerPrefs.GetInt("BestScore"))
        {
            PlayerPrefs.SetInt("BestScore", Score);
        }

        _inGame = false;
        _waitSecondDot = false;
        Destroy(_currentLine);
        Destroy(_bufferLine);
        Destroy(_player);
        Destroy(_obstacle);        
    }

    public void Pause()
    {
        _inGame = false;
        Time.timeScale = 0;
        
        UIController.singleton.ActivatePausePanel();
    }

    public void Continue()
    {
        _inGame = true;
        Time.timeScale = 1;
        
        UIController.singleton.ActivateGamePanel();
    }

	public void ShareScreenShotOnSocialNetwork ()
	{
		// Create share sheet
		ShareSheet _shareSheet 	= new ShareSheet();	
		_shareSheet.Text				= "My new score in \"Catch me\": "+Score;

		#if UNITY_ANDROID
			_shareSheet.URL=@"https://play.google.com/store/apps/details?id=com.Zulu.CatchMe";
		#elif UNITY_IOS
			_shareSheet.URL=@"https://itunes.apple.com/us/app/mountain-trials-racing/id1163546273";
		#endif

		_shareSheet.AttachScreenShot();

		// Show composer
		NPBinding.UI.SetPopoverPointAtLastTouchPosition();
		NPBinding.Sharing.ShowView(_shareSheet, FinishedSharing);
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL("https://play.google.com/store/apps/developer?id=ZULU");
		#elif UNITY_IOS
		Application.OpenURL("https://itunes.apple.com/us/developer/alexey-vasyutkin/id1163546273");
		#endif
	}


	private void FinishedSharing (eShareResult _result)
	{
		Debug.Log("Finished sharing");
		Debug.Log("Share Result = " + _result);
	}
}
