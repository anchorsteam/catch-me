﻿using UnityEngine;

public class Line : MonoBehaviour
{
    public GameObject Edge;

    public GameObject EdgeL;
    public GameObject EdgeR;

    private bool _inObject;

    void Start()
    {
        EdgeL = Instantiate(Edge, transform.position, Quaternion.identity) as GameObject;
        EdgeR = Instantiate(Edge, transform.position, Quaternion.identity) as GameObject;

        UpdateColor(0.6f);

        GetComponent<Collider2D>().isTrigger = true;        
    }

    public void SetLine()
    {
        GetComponent<Collider2D>().isTrigger = false;
        ColorManager.singletone.SetColor(gameObject, 1);
        if (_inObject)
            Destroy(gameObject);
    }

    public void SetRightEdge(Vector2 rightEdge)
    {
        if(EdgeR)
            EdgeR.transform.position = rightEdge;
    }

    void OnDestroy()
    {
        Destroy(EdgeL);
        Destroy(EdgeR);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        _inObject = true;
        GetComponent<SpriteRenderer>().color = new Color(235,0,0,1);
        EdgeL.GetComponent<SpriteRenderer>().color = new Color(235, 0, 0, 1);
        EdgeR.GetComponent<SpriteRenderer>().color = new Color(235, 0, 0, 1);
    }
    void OnTriggerExit2D(Collider2D other)
    {
        _inObject = false;
        UpdateColor(0.6f);
    }

    public void UpdateColor(float alpha)
    {
        ColorManager.singletone.SetColor(EdgeL, 1);
        ColorManager.singletone.SetColor(EdgeR, 1);

        ColorManager.singletone.SetColor(gameObject, alpha);
    }
}
