﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(ShopViewer))]
public class ShopViewerEditor : Editor
{
    private List<string> _categories;

    private int _selectedCategoty;
    private ShopViewer _myTarger;

    public void OnEnable()
    {
        _categories = new List<string> {"All"};
        
        _myTarger = (ShopViewer) target;
        if(_myTarger.shop!=null)
            _categories.AddRange(_myTarger.shop.GetComponent<Shop>().CategoriesToStringArray());
    }

    public override void OnInspectorGUI()
    {
        _myTarger.FindShop();

        if(_myTarger.shop==null)
            return;

        EditorGUILayout.LabelField("Categories:");

        _myTarger.categoryIndex = EditorGUILayout.Popup(_myTarger.categoryIndex+1,
            _categories.ToArray())-1;

        _myTarger.sellButton=EditorGUILayout.ObjectField("Sell button: ",_myTarger.sellButton,typeof(GameObject), true) as GameObject;
    }
}
