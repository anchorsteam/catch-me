﻿using System;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof (Shop))]
public class ShopEditor : Editor
{
    private ReorderableList _itemsList;
    private ReorderableList _categoriesList;

    private Shop myTarget;


    public void OnEnable()
    {
        _itemsList = new ReorderableList(serializedObject, serializedObject.FindProperty("items"), false, true, true,
            true);
        _categoriesList = new ReorderableList(serializedObject, serializedObject.FindProperty("categories"), false, true,
            true, true);

        myTarget = (Shop) target;

        if (Shop.mainShop == null)
            Shop.mainShop = myTarget;

        myTarget.Load();

        _itemsList.elementHeight = (EditorGUIUtility.singleLineHeight + 5)*8;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        _categoriesList.DoLayoutList();
        _itemsList.DoLayoutList();


        serializedObject.ApplyModifiedProperties();

        _categoriesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = _categoriesList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.LabelField(new Rect(rect.x, rect.y, 20, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("id").intValue.ToString());

            EditorGUI.PropertyField(new Rect(rect.x + 20, rect.y, rect.size.x - 20, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("name"), GUIContent.none);
        };

        _categoriesList.onAddCallback = (ReorderableList list) =>
        {
            var index = list.serializedProperty.arraySize;
            list.serializedProperty.arraySize++;
            list.index = index;

            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            element.FindPropertyRelative("name").stringValue = "<category name>";
            element.FindPropertyRelative("id").intValue = PlayerPrefs.GetInt("CategoryIDNumerator");
            PlayerPrefs.SetInt("CategoryIDNumerator", PlayerPrefs.GetInt("CategoryIDNumerator")+1);
        };

        _categoriesList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Categories");
        };

        _itemsList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = _itemsList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            var spacing = EditorGUIUtility.singleLineHeight + 5;

            EditorGUI.LabelField(new Rect(rect.x, rect.y, 60, EditorGUIUtility.singleLineHeight), "Name:");
            EditorGUI.PropertyField(new Rect(rect.x + 65, rect.y, rect.size.x - 65, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("name"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing, 60, EditorGUIUtility.singleLineHeight), "Price:");
            EditorGUI.PropertyField(
                new Rect(rect.x + 65, rect.y + spacing, rect.size.x - 65, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("price"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing*2, 60, EditorGUIUtility.singleLineHeight), "Item:");
            EditorGUI.PropertyField(
                new Rect(rect.x + 65, rect.y + spacing*2, rect.size.x - 65, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("item"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing*3, 60, EditorGUIUtility.singleLineHeight), "Icon:");
            EditorGUI.PropertyField(
                new Rect(rect.x + 65, rect.y + spacing*3, rect.size.x - 65, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("icon"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing*4, 60, EditorGUIUtility.singleLineHeight), "Bought:");
            EditorGUI.PropertyField(
                new Rect(rect.size.x + 5, rect.y + spacing*4, rect.size.x - 40, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("bought"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing *5, 60, EditorGUIUtility.singleLineHeight), "Selected:");
            EditorGUI.PropertyField(
                new Rect(rect.size.x + 5, rect.y + spacing * 5, rect.size.x - 40, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("selected"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing*6, 60, EditorGUIUtility.singleLineHeight), "ID:");
            EditorGUI.LabelField(new Rect(rect.size.x + 5, rect.y + spacing*6, 60, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("id").intValue.ToString());

            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing * 7, 60, EditorGUIUtility.singleLineHeight), "Category:");
            if (myTarget.categories.Count != 0)
            {
                if (myTarget.GetCategoryIndex(element.FindPropertyRelative("categoryID").intValue) == -1)
                    element.FindPropertyRelative("categoryID").intValue = myTarget.categories[0].id;

                element.FindPropertyRelative("categoryID").intValue =
                    myTarget.categories[
                        EditorGUI.Popup(new Rect(rect.x+65, rect.y + spacing*7, rect.size.x - 65, EditorGUIUtility.singleLineHeight),
                            myTarget.GetCategoryIndex(element.FindPropertyRelative("categoryID").intValue),
                            myTarget.CategoriesToStringArray())].id;
            }
            else
            {
                EditorGUI.Popup(new Rect(rect.x + 65, rect.y + spacing * 7, rect.size.x - 65, EditorGUIUtility.singleLineHeight),
                    0, new string[1] {"none"});
                element.FindPropertyRelative("categoryID").intValue = -1;
            }
            EditorGUI.LabelField(new Rect(rect.x, rect.y + spacing * 7+5, rect.size.x, EditorGUIUtility.singleLineHeight), "______________________________________________________");
        };

        _itemsList.onAddCallback = (ReorderableList list) =>
        {
            var index = list.serializedProperty.arraySize;
            list.serializedProperty.arraySize++;
            list.index = index;

            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            element.FindPropertyRelative("name").stringValue = "<Item name>";
            element.FindPropertyRelative("id").intValue = PlayerPrefs.GetInt("ItemIDCounter");
            element.FindPropertyRelative("categoryID").intValue = -1;
            PlayerPrefs.SetInt("ItemIDCounter", PlayerPrefs.GetInt("ItemIDCounter")+1);
        };

        _itemsList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Items");
        };
        myTarget.Save();
    }
}
