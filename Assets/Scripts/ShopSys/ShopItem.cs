﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class ShopItem
{
    public string name;
    public Sprite item;
    public Sprite icon;
    public int id;
    public int price;
    public bool bought;
    public int categoryID;
    public bool selected;

    public string Description()
    {
        if (!bought)
            return price.ToString();
        else if (selected)
            return "Selected";
        else
            return "Use";
    }

    public Sprite Icon()
    {
        if (!bought)
            return icon;
        else
            return item;
    }
}
