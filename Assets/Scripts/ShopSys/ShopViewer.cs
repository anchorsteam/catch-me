﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[Serializable]
public class ShopViewer : MonoBehaviour
{
    public GameObject sellButton;

    public int categoryIndex;
    public Shop shop;

    private List<GameObject> _buyButtons; 

    void Awake()
    {
        if(shop==null)
            FindShop();

        _buyButtons=new List<GameObject>();
        
    }
    void Start()
    {
        Shop.mainShop.OnBuying += delegate(ShopItem item) 
        {
            UpdateButtonsInfo();
            UIController.singleton.UpdateCoinsCount();
            
        };

        Shop.mainShop.Load();
        FillPanel();
    }

    public void FillPanel()
    {
        var tempItems = (categoryIndex >= 0)
            ? shop.items.FindAll(pred => pred.categoryID ==shop.categories[categoryIndex].id)
            : shop.items;

        for (int index = 0; index < tempItems.Count; index++)
        {
            var tempButton = Instantiate(sellButton) as GameObject;

            var id = tempItems[index].id;
            tempButton.GetComponent<Button>().onClick.AddListener(delegate
            {
                GameController.singleton.PlayButtonSound();
                shop.BuyItem(id);
            });
            tempButton.transform.SetParent(transform,false);
            _buyButtons.Add(tempButton);
        }
        UpdateButtonsInfo();

        transform.position=new Vector3(shop.items.Count*100,0);
    }

    public void FindShop()
    {
        shop = GameObject.FindGameObjectWithTag("Shop").GetComponent<Shop>();
    }

    public void UpdateButtonsInfo()
    {
        var tempItems = (categoryIndex >= 0)
            ? shop.items.FindAll(pred => pred.categoryID == shop.categories[categoryIndex].id)
            : shop.items;

        for (var index = 0; index < _buyButtons.Count; index++)
        {
            _buyButtons[index].name = tempItems[index].name;
            _buyButtons[index].transform.FindChild("Icon").GetComponent<Image>().sprite = tempItems[index].Icon();
            _buyButtons[index].transform.FindChild("Icon").GetComponent<Image>().color =
                ColorManager.singletone.currentColor.inversionColor;

            _buyButtons[index].transform.FindChild("Description").GetComponent<Text>().text = tempItems[index].Description();
            _buyButtons[index].transform.FindChild("Description").GetComponent<Text>().color =
                ColorManager.singletone.currentColor.inversionColor;
        }
    }
}