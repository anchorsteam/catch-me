﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

[Serializable]
public struct Category
{
    public string name;
    public int id;

    public override string ToString()
    {
        return name;
    }
}


public delegate void BuyingCallBack(ShopItem item);

public delegate void WithdrawMoneyCallback(int itemPrice);


public class Shop : MonoBehaviour
{
    public List<ShopItem> items;
    public List<Category> categories;

    public event BuyingCallBack OnBuying;
    public event WithdrawMoneyCallback OnWithdrawMoney;

    public static int money;

    public static Shop mainShop;

    void Awake()
    {
        mainShop = this;

        if (PlayerPrefs.GetInt("LounchCount") == 0)
        {
            items[0].bought = true;
            items[0].selected = true;
            PlayerPrefs.SetInt("LounchCount", PlayerPrefs.GetInt("LounchCount") + 1);
            Save();
        }

        Load();

        OnWithdrawMoney += delegate(int price)
        {
            
            money -= price;
        };
    }

    public int GetCategoryIndex(int id)
    {
        return categories.IndexOf(categories.Find(x => x.id == id));
    }


    public string[] CategoriesToStringArray()
    {
        var temp = new string[categories.Count];
        for (var index = 0; index < categories.Count; index++)
        {
            temp[index] = categories[index].ToString();
        }
        return temp;
    }

    public List<ShopItem> GetItemsInCategory(int categoryID)
    {
        return items.FindAll(pred => pred.categoryID == categoryID);
    }

    public List<ShopItem> GetItemsInCategory(string categoryName)
    {
        return items.FindAll(pred => pred.categoryID == categories.Find(catPred => catPred.name == categoryName).id);
    }

    public void BuyItem(int id)
    {
        var item = items.Find(pred => pred.id == id);

        if(money-item.price<0&&!item.bought)
            return;

        if (OnWithdrawMoney != null && !item.bought)
            OnWithdrawMoney(item.price);

        item.bought = true;

        foreach (var tempItem in Shop.mainShop.items)
            tempItem.selected = false;
        item.selected = true;

        Save();
        if (OnBuying != null)
            OnBuying(item);
    }

    public void Save()
    {
        PlayerPrefs.SetInt("Money", money);
        foreach (var item in items)
        {
            PlayerPrefs.SetInt("ItemBought" + item.name + item.id, item.bought ? 1 : 0);
            PlayerPrefs.SetInt("ItemSelected" + item.name + item.id, item.selected ? 1 : 0);
        }
    }

     public void Load()
     {
         money = PlayerPrefs.GetInt("Money");
         foreach (var item in items)
         {
             item.bought = PlayerPrefs.GetInt("ItemBought" + item.name + item.id) == 1;
             item.selected = PlayerPrefs.GetInt("ItemSelected" + item.name + item.id) == 1;
         }
     }

    void OnDestroy()
    {
        Save();
    }
}
 