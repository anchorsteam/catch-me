﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject MainMenuPanel;
    public Image CatchMeLogo;
    public Image PlayButton;
	public Image MoreGamesButton;

    public GameObject GamePanel;
    public Text ScoreText;

    public GameObject GameOverPanel;
    public Text LastScore;
    public Text BestScore;
    public Image GameOverImage;
    public Image RestartImage;
    public Image ShopImage;
    public Image LeaderboardImage;

    public GameObject ShopPanel;
    public Image ShopLogoImage;
    public Text BackButton;
    public GameObject AddFreeCoins;
    public Image AddFreeCoinsImage;
    public Text AddFreeCoinsText;

    public GameObject CoinsPanel;
    public Image CoinImage;
    public Text CointCount;

    public Image Logo;
    public float LogoTimer;

    public GameObject PausePanel;
    public Image PauseImage;
    public Image ContinueButton;

    [HideInInspector]
    public static UIController singleton;

    void Awake()
    {
        singleton = this;
    }

    void Start ()
    {
        UpdateUIColor();
        UpdateCoinsCount();
        ActivateLogo();
    }

    void Update()
    {
        if(LogoTimer<0&&Logo.gameObject.activeInHierarchy)
            ActivateMainMenuPanel();
        else if(Logo.gameObject.activeInHierarchy)
            LogoTimer -= Time.deltaTime;
    }

    public void ActivateLogo()
    {
        ShopPanel.SetActive(false);
        GamePanel.SetActive(false);
        GameOverPanel.SetActive(false);
        MainMenuPanel.SetActive(false);
        CoinsPanel.SetActive(false);
        Logo.gameObject.SetActive(true);
        PausePanel.SetActive(false);
		AppodealController.Instance.showBanner ();
    }

    public void ActivateGamePanel()
    {
        GamePanel.SetActive(true);
        GameOverPanel.SetActive(false);
        MainMenuPanel.SetActive(false);
        CoinsPanel.SetActive(true);
        Logo.gameObject.SetActive(false);
        PausePanel.SetActive(false);

        CoinImage.color = new Color(CoinImage.color.r, CoinImage.color.g, CoinImage.color.b, 0.6f);
        CointCount.color = new Color(CointCount.color.r, CointCount.color.g, CointCount.color.b, 0.6f);

		AppodealController.Instance.hideBanner ();
    }

    public void ActivateGameOverPanel()
    {
        GameOverPanel.SetActive(true);
        GamePanel.SetActive(false);
        MainMenuPanel.SetActive(false);
        ShopPanel.SetActive(false);
        CoinsPanel.SetActive(true);
        Logo.gameObject.SetActive(false);
        PausePanel.SetActive(false);

        CoinImage.color = new Color(CoinImage.color.r, CoinImage.color.g, CoinImage.color.b, 1);
        CointCount.color = new Color(CointCount.color.r, CointCount.color.g, CointCount.color.b, 1);

		if (PlayerPrefs.GetInt ("GameCount") % 3 == 0)
			AppodealController.Instance.showInterstitialOrVideo ();

		AppodealController.Instance.showBanner ();
		PlayerPrefs.SetInt ("GameCount", PlayerPrefs.GetInt ("GameCount") + 1);
    }

    public void CheckScore(int lastScore, int bestScore)
    {
        if (lastScore > bestScore)
        {
            BestScore.text = "NEW BEST SCORE!";
            LastScore.text = "SCORE: " + lastScore;
        }
        else
        {
            BestScore.text = "BEST SCORE: " + bestScore;
            LastScore.text = "LAST SCORE: " + lastScore;
        }

    }

    public void ActivateMainMenuPanel()
    {
        MainMenuPanel.SetActive(true);
        GameOverPanel.SetActive(false);
        GamePanel.SetActive(false);   
        ShopPanel.SetActive(false);
        CoinsPanel.SetActive(false);
        Logo.gameObject.SetActive(false);
        PausePanel.SetActive(false);
		AppodealController.Instance.showBanner ();
    }

    public void UpdateScore(int score)
    {
        ScoreText.text = score.ToString();
    }

    public void UpdateUIColor()
    {
        ScoreText.color =new Color (ColorManager.singletone.currentColor.inversionColor.r, ColorManager.singletone.currentColor.inversionColor.g, ColorManager.singletone.currentColor.inversionColor.b,0.6f);        

        LastScore.color = ColorManager.singletone.currentColor.inversionColor;
        BestScore.color = ColorManager.singletone.currentColor.inversionColor;
        GameOverImage.color = ColorManager.singletone.currentColor.inversionColor;
        RestartImage.color = ColorManager.singletone.currentColor.inversionColor;
        ShopImage.color = ColorManager.singletone.currentColor.inversionColor;
        LeaderboardImage.color = ColorManager.singletone.currentColor.inversionColor;

        CatchMeLogo.color = ColorManager.singletone.currentColor.inversionColor;
        PlayButton.color = ColorManager.singletone.currentColor.inversionColor;
		MoreGamesButton.color = ColorManager.singletone.currentColor.inversionColor;

        ShopLogoImage.color = ColorManager.singletone.currentColor.inversionColor;
        BackButton.color = ColorManager.singletone.currentColor.inversionColor;
        AddFreeCoinsImage.color = ColorManager.singletone.currentColor.inversionColor;
        AddFreeCoinsText.color = ColorManager.singletone.currentColor.inversionColor;

        CoinImage.color=new Color(ColorManager.singletone.currentColor.inversionColor.r,ColorManager.singletone.currentColor.inversionColor.g,ColorManager.singletone.currentColor.inversionColor.b, CoinImage.color.a);
        CointCount.color = new Color(ColorManager.singletone.currentColor.inversionColor.r, ColorManager.singletone.currentColor.inversionColor.g, ColorManager.singletone.currentColor.inversionColor.b, CointCount.color.a);

        Logo.color = ColorManager.singletone.currentColor.inversionColor;

        PauseImage.color = ColorManager.singletone.currentColor.inversionColor;
        ContinueButton.color = ColorManager.singletone.currentColor.inversionColor;
    }

    public void ActivateShopPanel()
    {
        MainMenuPanel.SetActive(false);
        GameOverPanel.SetActive(false);
        GamePanel.SetActive(false);
        ShopPanel.SetActive(true);
        CoinsPanel.SetActive(true);
        Logo.gameObject.SetActive(false);
		AddFreeCoins.SetActive (AppodealController.Instance.hasNonSkippableVideo);

        CoinImage.color = new Color(CoinImage.color.r, CoinImage.color.g, CoinImage.color.b,1);
        CointCount.color = new Color(CointCount.color.r, CointCount.color.g, CointCount.color.b, 1);
		AppodealController.Instance.hideBanner ();
    }

    public void UpdateCoinsCount()
    {
        CointCount.text = Shop.money.ToString();
    }

    public void ActivatePausePanel()
    {
        PausePanel.SetActive(true);
        GamePanel.SetActive(false);
        CoinsPanel.SetActive(true);
        GameOverPanel.SetActive(false);
        MainMenuPanel.SetActive(false);
		AppodealController.Instance.showBanner ();
    }

}
