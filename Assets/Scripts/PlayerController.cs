﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform Direction;

    public float StartSpeed;
    public float MaxSpeed;
    public float Step;
    public GameObject Sparks;

    private Rigidbody2D _rb;
    private Vector2 _oldSpeed;

    private AudioSource[] _knocksAudioSources;

    
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _knocksAudioSources = GetComponents<AudioSource>();
        
    }

	void Start ()
    {
        Direction.rotation = Quaternion.Euler(0, 0, Random.Range(0,360));
        Direction.Translate(StartSpeed, 0, 0);
        _rb.AddForce(Direction.position, ForceMode2D.Impulse);

        ColorManager.singletone.SetColor(gameObject, 1);

	    GetComponent<SpriteRenderer>().sprite = Shop.mainShop.items.Find(x => x.selected).item;
    }

    void Update()
    {
        var xPos = Camera.main.WorldToScreenPoint(transform.position).x;
        var yPos = Camera.main.WorldToScreenPoint(transform.position).y;

        if (xPos < -20 || yPos < -20 || xPos > Screen.width + 20 || yPos > Screen.height + 20)
            GameController.singleton.GameOver();

        if (Input.GetKeyDown(KeyCode.Space))
            Instantiate(Sparks);

       
    }

    void FixedUpdate()
    {
        _oldSpeed = _rb.velocity;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        _rb.velocity = Vector2.Reflect(_oldSpeed, other.contacts[0].normal);

        if (_rb.velocity.magnitude < MaxSpeed)
            _rb.velocity += other.contacts[0].normal * Step;

        GameController.singleton.AddPoint();

        Instantiate(Sparks, other.contacts[0].point*0.9f, Quaternion.Euler(-_angleBetweenVector2(other.contacts[0].point, transform.position), 90,0));

        _knocksAudioSources[Random.Range(0,_knocksAudioSources.Length)].Play();
    }


    float _angleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        var diference = vec2 - vec1;
        var sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }
    
}
