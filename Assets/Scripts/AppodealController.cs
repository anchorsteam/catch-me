﻿using System;
using UnityEngine;
using UnityEngine.UI;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

// Example script showing how to invoke the Appodeal Ads Unity plugin.
public class AppodealController : MonoBehaviour, IInterstitialAdListener, IBannerAdListener, INonSkippableVideoAdListener, ISkippableVideoAdListener, IRewardedVideoAdListener, IPermissionGrantedListener
{
	public static AppodealController Instance;
	public bool hasNonSkippableVideo;
	#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
		string appKey = "";
	#elif UNITY_ANDROID
	string appKey = "a8cb6875eba4a354b1d7742d3adcc3ffb178022714959a5d";
	#elif UNITY_IPHONE
	string appKey = "7bef7b4f59dbf66311f1dc5ccfa78ad6b13fc76d8e9d0c41";
	#else
		string appKey = "";
	#endif

	public bool LoggingToggle, TestingToggle, ConfirmToggle;

	void Awake ()
	{
		Instance = this;
		Appodeal.requestAndroidMPermissions(this);
		Init ();
	}

	public void Init() {
		//Example for UserSettings usage
		UserSettings settings = new UserSettings ();
		settings.setUserId("1234567890").setAge(25).setBirthday ("01/01/1990").setAlcohol(UserSettings.Alcohol.NEUTRAL)
			    .setSmoking(UserSettings.Smoking.NEUTRAL).setEmail("hi@appodeal.com")
				.setGender(UserSettings.Gender.OTHER).setRelation(UserSettings.Relation.DATING)
				.setInterests("gym, cars, cinema, science").setOccupation(UserSettings.Occupation.WORK);
		
		if (LoggingToggle) Appodeal.setLogging(true);
		if (TestingToggle) Appodeal.setTesting(true);
		if (ConfirmToggle) Appodeal.confirm(Appodeal.SKIPPABLE_VIDEO);

		Appodeal.setSmartBanners(false);

		Appodeal.initialize (appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.SKIPPABLE_VIDEO | Appodeal.NON_SKIPPABLE_VIDEO);

		Appodeal.setBannerCallbacks (this);
		Appodeal.setInterstitialCallbacks (this);
		Appodeal.setSkippableVideoCallbacks (this);
		//Appodeal.setRewardedVideoCallbacks(this);
		Appodeal.setNonSkippableVideoCallbacks(this);

		Appodeal.setCustomRule("newBoolean", true);
		Appodeal.setCustomRule("newInt", 1234567890);
		Appodeal.setCustomRule("newDouble", 123.123456789);
		Appodeal.setCustomRule("newString", "newStringFromSDK");
	}

	public void showInterstitial() {
		Appodeal.show (Appodeal.INTERSTITIAL, "interstitial_button_click");
	}

	public void showSkippableVideo() {
		Appodeal.show (Appodeal.SKIPPABLE_VIDEO, "skippable_video_button_click");
	}

	public void showRewardedVideo() {
		Appodeal.show (Appodeal.NON_SKIPPABLE_VIDEO);
	}

	public void showBanner() {
		Appodeal.show (Appodeal.BANNER_TOP, "banner_button_click");
	}

	public void showInterstitialOrVideo() {
		Appodeal.show (Appodeal.INTERSTITIAL | Appodeal.SKIPPABLE_VIDEO);
	}

	public void hideBanner() {
		Appodeal.hide (Appodeal.BANNER);
	}

	#region Banner callback handlers

	public void onBannerLoaded() { Debug.Log("Banner loaded"); }
	public void onBannerFailedToLoad() { Debug.Log("Banner failed"); }
	public void onBannerShown() { Debug.Log("Banner opened"); }
	public void onBannerClicked() { Debug.Log("banner clicked"); }

	#endregion

	#region Interstitial callback handlers
	
	public void onInterstitialLoaded() { Debug.Log("Interstitial loaded"); }
	public void onInterstitialFailedToLoad() { Debug.Log("Interstitial failed"); }
	public void onInterstitialShown() { Debug.Log("Interstitial opened"); }
	public void onInterstitialClicked() { Debug.Log("Interstitial clicked"); }
	public void onInterstitialClosed() { Debug.Log("Interstitial closed"); }
	
	#endregion

	#region Video callback handlers

	public void onSkippableVideoLoaded() { Debug.Log("Skippable Video loaded"); }
	public void onSkippableVideoFailedToLoad() { Debug.Log("Skippable Video failed"); }
	public void onSkippableVideoShown() { Debug.Log("Skippable Video opened"); }
	public void onSkippableVideoClosed() { Debug.Log("Skippable Video closed"); }
	public void onSkippableVideoFinished() { Debug.Log("Skippable Video finished"); }

	#endregion

	#region Non Skippable Video callback handlers
	
	public void onNonSkippableVideoLoaded() { hasNonSkippableVideo = true; }
	public void onNonSkippableVideoFailedToLoad() { hasNonSkippableVideo = false; }
	public void onNonSkippableVideoShown() { Debug.Log("NonSkippable Video opened"); }
	public void onNonSkippableVideoClosed() { Debug.Log("NonSkippable Video closed"); }
	public void onNonSkippableVideoFinished() 
	{ 
		Invoke ("AddMoney", 0.5f);
	}
	void AddMoney()
	{
	Shop.money += 15;
	Shop.mainShop.Save ();
	UIController.singleton.UpdateCoinsCount ();
	}
	#endregion

	#region Rewarded Video callback handlers
	
	public void onRewardedVideoLoaded() { Debug.Log("Rewarded Video loaded"); }
	public void onRewardedVideoFailedToLoad() { Debug.Log("Rewarded Video failed"); }
	public void onRewardedVideoShown() { Debug.Log("Rewarded Video opened"); }
	public void onRewardedVideoClosed() { Debug.Log("Rewarded Video closed"); }
	public void onRewardedVideoFinished(int amount, string name) 
	{ 
		
	}
	
	#endregion

	#region Permission Grant callback handlers
	
	public void writeExternalStorageResponse(int result) { 
		if(result == 0) {
			Debug.Log("WRITE_EXTERNAL_STORAGE permission granted"); 
		} else {
			Debug.Log("WRITE_EXTERNAL_STORAGE permission grant refused"); 
		}
	}
	public void accessCoarseLocationResponse(int result) { 
		if(result == 0) {
			Debug.Log("ACCESS_COARSE_LOCATION permission granted"); 
		} else {
			Debug.Log("ACCESS_COARSE_LOCATION permission grant refused"); 
		}
	}
	
	#endregion
}